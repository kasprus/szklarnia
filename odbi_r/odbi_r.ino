#include <LiquidCrystal.h>
//#include <NewPing.h>

#include <RCSwitch.h>
#define PIN_ALARMOWY 9
#define TEMPERATURA_KRYTYCZNA 350
//#define TRIGGER_PIN  10
//#define ECHO_PIN     11
//#define MAX_DISTANCE 200
//bool wlaczany; 
//NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);
RCSwitch mySwitch = RCSwitch();
LiquidCrystal lcd(3, 4, 5, 6, 7, 8);
void wypisz();
void setup() {
  //Serial.begin(9600);
  lcd.begin(16,2);
  mySwitch.enableReceive(0);  // Receiver on interrupt 0 => that is pin #2
  pinMode(PIN_ALARMOWY, OUTPUT);
  wypisz();
}
long int value;
bool zmiana=LOW;
int temperatura;
bool pora;
int wilgotnosc;
long int nr;
int wlaczenie;
int wylaczenie;

void wypisz(){
 // if(wlaczany==0)lcd.noDisplay();
//  else{
   //lcd.display();
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("T:");
  lcd.print((float)temperatura/10.0);
  lcd.print(" W:");
  lcd.print(wilgotnosc);
  lcd.print("%");
  lcd.setCursor(0,1);
  if(pora==0)lcd.print("DZIEN ");
  else{
    lcd.print("NOC ");
  }
  /*lcd.print(sonar.ping_cm());
  lcd.print("cm");*/
  lcd.print(wlaczenie);
  lcd.print(" ");
  lcd.print(wylaczenie);
//  }
}

//int odl;

void loop() {
  if(temperatura>TEMPERATURA_KRYTYCZNA){
    zmiana=!zmiana;
  }
  else{
    zmiana=LOW;
  }
  /*odl=sonar.ping_cm();
  if(odl!=0){
    wlaczany=1;
    wypisz();
  }
  else{
    wlaczany=0;
    wypisz();
  }*/
  digitalWrite(PIN_ALARMOWY, zmiana);
  if (mySwitch.available()) {
    
    value = mySwitch.getReceivedValue();
    //if(value!=0){
      nr=value/2000;
      value=value-nr*2000;

      /*Serial.print(nr);
      Serial.print(" ");
      if(nr==2)Serial.print(value/10.0);
      else{
        Serial.print(value);
      }
      Serial.print("\n");
    //}*/
      if(nr==0&&wilgotnosc!=value){
        wilgotnosc=value;
        wypisz();
      }
      if(nr==1&&pora!=value){
        pora=value;
        wypisz();
      }
      if(nr==2&&temperatura!=value){
        temperatura=value;
        wypisz();
      }
      if(nr==3&&wlaczenie!=value){
        wlaczenie=value;
        wypisz();
      }
      if(nr==4&&wylaczenie!=value){
        wylaczenie=value;
        wypisz();
      }
      

      
    mySwitch.resetAvailable();
    
  }
  /*else{
    Serial.print("blad\n");
  }*/
  delay(1000);
}
