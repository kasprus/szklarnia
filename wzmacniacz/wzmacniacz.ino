#ifndef _NADAJNIK
#define _NADJANIK
#include <RCSwitch.h>

class NADAJNIK{
  public:
  
  NADAJNIK(int pin);
  ~NADAJNIK();
  void wyslij(int nr, int a);
  private:
  RCSwitch *mySwitch;

};
NADAJNIK::NADAJNIK(int pin){
  mySwitch=new RCSwitch();
  mySwitch->enableTransmit(pin);
}

NADAJNIK::~NADAJNIK(){
  delete mySwitch;
}

void NADAJNIK::wyslij(int nr, int a){
  nr=nr*2000+a;
  /*Serial.print(a);
  Serial.print("\n");
  Serial.print(nr);
  Serial.print("\n");*/
  mySwitch->send(nr, 24);
}

#endif
#define PIN_NADAJNIKA 10
NADAJNIK nadajnik(PIN_NADAJNIKA);
RCSwitch odbiornik = RCSwitch();

void setup() {
  //Serial.begin(9600);
  odbiornik.enableReceive(0);  // Receiver on interrupt 0 => that is pin #2
  pinMode(13, OUTPUT);
}
int value;
void loop(){
    if (odbiornik.available()) {
      digitalWrite(13, HIGH);
    noInterrupts();
    value = odbiornik.getReceivedValue();
    if(value>=0&&value <=100 || value==2000 ||value==2001 || value>=4000 &&value <= 4600 || value >=6000 && value <=6100 || value >=8000 && value <=8100){
    nadajnik.wyslij(value, 24);
    //delay(50);
    }
    odbiornik.resetAvailable();
    digitalWrite(13, LOW);
    interrupts();
}
}
