#include <DHT.h>
#include <DHT_U.h>

int DLUGOSC_WLACZENIA_W_NOCY=5;
int DLUGOSC_WLACZENIA_W_DZIEN=10;
int DLUGOSC_WYLACZENIA_W_NOCY=10;
int DLUGOSC_WYLACZENIA_W_DZIEN=10;

#ifndef LCDDISPLAY
#define LCDDISPLAY

#include<LiquidCrystal.h>

//domyślne numery pinów wyświetlacza


#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

class LCD_DISPLAY{
  public:
    LiquidCrystal *lcd;
    //LCD_DISPLAY();
    LCD_DISPLAY(int pin1, int pin, int pin3, int pin4, int pin5, int pin6, int pina);
    ~LCD_DISPLAY();
    void read_button();//sprawdzenie jaki przycisk
    int give_button();//zwraca numer przycisku
    //bool give_active();//zwraca, czy wciśnięcie aktywne
  private:
    int key;
    int adc_key_in;
    int prev;
    int nr_analog;
  };

LCD_DISPLAY::LCD_DISPLAY(int pin1, int pin2, int pin3, int pin4, int pin5, int pin6, int pina){
  lcd=new LiquidCrystal(pin1, pin2, pin3, pin4, pin5, pin6, pina);
  key=btnNONE;
  nr_analog=pina;
  lcd->begin(16,2);
  lcd->setCursor(0,0);
}

LCD_DISPLAY::~LCD_DISPLAY(){
  delete lcd;
}

void LCD_DISPLAY::read_button(){
 adc_key_in=analogRead(nr_analog);
 prev=key;
 if (adc_key_in < 50)   key=btnRIGHT;  
 else if (adc_key_in < 195)  key=btnUP; 
 else if (adc_key_in < 380)  key=btnDOWN; 
 else if (adc_key_in < 555)  key=btnLEFT; 
 else if (adc_key_in < 790)  key=btnSELECT;
 else if (adc_key_in >=790)  key=btnNONE;
}

int LCD_DISPLAY::give_button(){
  if(prev==key)return btnNONE;
  return key;
}


#endif

//czujnik temperatury
#ifndef CZUJNIK_WIT
#define CZUJNIK_WIT


#define DHTTYPE DHT11

class CZUJNIKWIT{
  public:
  CZUJNIKWIT(int pin);
  ~CZUJNIKWIT();
  void odczyt();
  int temperatura();
  int wilgotnosc();
  DHT *dht;
  private:
  int temp;
  int wil;
  
};

CZUJNIKWIT::CZUJNIKWIT(int pin){
  dht=new DHT(pin, DHTTYPE);
  dht->begin();
  temp=-99;
  wil=-99;
}

CZUJNIKWIT::~CZUJNIKWIT(){
  delete dht;
}

void CZUJNIKWIT::odczyt(){
  float a=dht->readTemperature();
  temp=isnan(a)?(-99):a;
  a=dht->readHumidity();
  wil=isnan(a)?(-99):a;
}

int CZUJNIKWIT::temperatura(){
  return temp;
}

int CZUJNIKWIT::wilgotnosc(){
  return wil;
}
#endif

#ifndef CZUJNIK_SWIATLA
#define CZUJNIK_SWIATLA

class CZUJNIKSWIATLA{

  public:
  char *pora();
  void odczyt();
  CZUJNIKSWIATLA(int pin);
  bool _pora();
  private:
  char *noc="NOC";
  char *dzien="DZIEN";
  int pin;
  bool stan; //0-dzien, 1-noc
};

CZUJNIKSWIATLA::CZUJNIKSWIATLA(int pin){
  pinMode(pin, INPUT);
  this->pin=pin;
  stan=0;
}

void CZUJNIKSWIATLA::odczyt(){
  stan=digitalRead(pin);
}

char* CZUJNIKSWIATLA::pora(){
  if(stan==0)return dzien;
  return noc;
}

bool CZUJNIKSWIATLA::_pora(){
  return stan;
}

#endif

#ifndef _POTENCJOMETR
#define _POTENCJOMETR

class POTENCJOMETR{
  public:
  POTENCJOMETR(int pin);
  int procent();
  void odczyt();
  private:
  int wartosc;
  int pin;
};

POTENCJOMETR::POTENCJOMETR(int pin){
  this->pin=pin;
  wartosc=analogRead(pin);
}

void POTENCJOMETR::odczyt(){
  wartosc=analogRead(pin);
}

int POTENCJOMETR::procent(){
  if(wartosc>900)return 100;
  return wartosc/9;
}

#endif
#ifndef _BUZZER
#define _BUZZER

class BUZZER{
  
  public:
  BUZZER(int pin);
  void wlacz();
  void wylacz();
  
  private:
  int pin;
};

BUZZER::BUZZER(int pin){
  this->pin=pin;
  pinMode(pin, OUTPUT);
}

void BUZZER::wlacz(){
  digitalWrite(pin, HIGH);
}

void BUZZER::wylacz(){
  digitalWrite(pin, LOW);
}
#endif

#ifndef _NADAJNIK
#define _NADJANIK
#include <RCSwitch.h>

class NADAJNIK{
  public:
  
  NADAJNIK(int pin);
  ~NADAJNIK();
  void wyslij(int nr, int a);
  private:
  RCSwitch *mySwitch;

};
NADAJNIK::NADAJNIK(int pin){
  mySwitch=new RCSwitch();
  mySwitch->enableTransmit(pin);
}

NADAJNIK::~NADAJNIK(){
  delete mySwitch;
}

void NADAJNIK::wyslij(int nr, int a){
  nr=nr*2000+a;
  /*Serial.print(a);
  Serial.print("\n");
  Serial.print(nr);
  Serial.print("\n");*/
  mySwitch->send(nr, 24);
}

#endif

#ifndef _TERMOMETR
#define _TERMOMETR

#include <OneWire.h>
#include <DallasTemperature.h>

class TERMOMETR{
  public:
  TERMOMETR(int pin);
  //~TERMOMETR();
  void odczyt();
  int podaj_temperature_10();
  private:
 // int licznik;
  int temperatura_10;
  OneWire oneWire;
  DallasTemperature sensors;
};

TERMOMETR::TERMOMETR(int pin) : oneWire(pin), sensors(&oneWire) {
  temperatura_10=0;
  sensors.begin();
//  licznik=0;
 // oneWire=OneWire(pin);
  //sensors(&oneWire);
}

void TERMOMETR::odczyt(){
  //++licznik;
  //licznik=licznik%10;
  //if(licznik==0){
  sensors.requestTemperatures();
  temperatura_10=(10*sensors.getTempCByIndex(0));
  //}
}

int TERMOMETR::podaj_temperature_10(){
  return temperatura_10;
}


#endif

#ifndef _PRZEKAZNIK
#define _PRZEKAZNIK

class PRZEKAZNIK{
  public:
  PRZEKAZNIK(int pin1, int pin2);
  void wlacz1();
  void wlacz2();
  void wylacz1();
  void wylacz2();
  bool stan1();
  bool stan2();
  private:
  bool wlaczany1;
  bool wlaczany2;
  int pin1;
  int pin2;
};

PRZEKAZNIK::PRZEKAZNIK(int pin1, int pin2){
  this->pin1=pin1;
  this->pin2=pin2;
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  wlaczany1=wlaczany2=0;
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
}

void PRZEKAZNIK::wlacz1(){
  digitalWrite(pin1, HIGH);
  wlaczany1=1;
}

void PRZEKAZNIK::wlacz2(){
  digitalWrite(pin2, HIGH);
  wlaczany2=1;
}


void PRZEKAZNIK::wylacz1(){
  digitalWrite(pin1, LOW);
  wlaczany1=0;
}


void PRZEKAZNIK::wylacz2(){
  digitalWrite(pin2, LOW);
  wlaczany2=0;
}

bool PRZEKAZNIK::stan1(){
  return wlaczany1;
}

bool PRZEKAZNIK::stan2(){
  return wlaczany2;
}

#endif

#ifndef _TRYB
#define _TRYB

class TRYB{
  public:
  TRYB(int a, int b, int c):tryb(0){
    pinMode(a, OUTPUT);
    pinMode(b, OUTPUT);
    pinMode(c, OUTPUT);
    tab[0]=a;
    tab[1]=b;
    tab[2]=c;
    }
  void plus();
  void minus();
  int stan();
  private:
  int tab[3];
  int tryb;
  
};

void TRYB::plus(){
  int a=0;  
  ++tryb;
  tryb=tryb%4;
  while(a<3){
    if(a<tryb)digitalWrite(tab[a], HIGH);
    else{
      digitalWrite(tab[a], LOW);
    }
    ++a;
  }

}

void TRYB::minus(){
  int a=0;
  --tryb;
  if(tryb<0)tryb=3;
  //tryb=tryb%4;
    while(a<3){
    if(a<tryb)digitalWrite(tab[a], HIGH);
    else{
      digitalWrite(tab[a], LOW);
    }
    ++a;
  }

}

int TRYB::stan(){
  return tryb;
}

#endif

#ifndef _POZIOM
#define _POZIOM

class POZIOM{
  public:      
  void plus();
  void minus();
  void zeruj();
  void wyswietl();
  POZIOM(int a, int b, int c, int d, int e, int f, int g, int h){

    pinMode(a, OUTPUT);
    pinMode(b, OUTPUT);
    pinMode(c, OUTPUT);
    pinMode(d, OUTPUT);
    pinMode(e, OUTPUT);
    pinMode(f, OUTPUT);
    pinMode(g, OUTPUT);
    pinMode(h, OUTPUT);
    tab[0]=a;
    tab[1]=b;
    tab[2]=c;
    tab[3]=d;
    tab[4]=e;
    tab[5]=f;
    tab[6]=g;
    tab[7]=h;
    tryb=0;
  }
  int stan();
  private:
  /*int pin1;
  int pin2;
  int pin3;
  int pin4;
  int pin5;
  int pin6;
  int pin7;
  int pin8;*/
  int tryb;
  int tab[9];
};

void POZIOM::plus(){
  int a=0;  
  ++tryb;
  tryb=tryb%9;
  while(a<8){
    if(a<tryb)digitalWrite(tab[a], HIGH);
    else{
      digitalWrite(tab[a], LOW);
    }
    ++a;
  }

}

void POZIOM::wyswietl(){
  int a=0;
  while(a<8){
    if(a<tryb)digitalWrite(tab[a], HIGH);
    else{
      digitalWrite(tab[a], LOW);
    }
    ++a;
  }
}

void POZIOM::minus(){
  int a=0;  
  --tryb;
  //tryb=tryb%9;
  if(tryb<0)tryb=9;
  while(a<8){
    if(a<tryb)digitalWrite(tab[a], HIGH);
    else{
      digitalWrite(tab[a], LOW);
    }
    ++a;
  }

}

void POZIOM::zeruj(){
  int a=0;
  tryb=0;
  while(a<8){
    digitalWrite(tab[a], LOW);
    ++a;
  }
}
int POZIOM::stan(){
  return tryb;
}
#endif

#ifndef _MENU
#define _MENU

class MENU{

  public:
  MENU(int pin1, int pin2, int pin3, int pin11, int pin22, int pin33, int pin44, int pin55, int pin66, int pin77, int pin88, int pin111, int pin222, int pin333) : tryb(pin1, pin2, pin3), poziom00(pin11, pin22, pin33, pin44, pin55, pin66, pin77, pin88), poziom11(pin11, pin22, pin33, pin44, pin55, pin66, pin77, pin88), poziom22(pin11, pin22, pin33, pin44, pin55, pin66, pin77, pin88), poziom33(pin11, pin22, pin33, pin44, pin55, pin66, pin77, pin88), pin_plus(pin111), pin_minus(pin222), pin_tryb(pin333){
    pinMode(pin_plus, INPUT_PULLUP);
    pinMode(pin_minus, INPUT_PULLUP);
    pinMode(pin_tryb, INPUT_PULLUP);
  }
  void przycisk();
  int poziom1();
  int poziom2();
  int poziom3();

  private:

  TRYB tryb;
  POZIOM poziom00;
  POZIOM poziom11;
  POZIOM poziom22;
  POZIOM poziom33;
  int pin_plus;
  int pin_minus;
  int pin_tryb;
  
};
void MENU::przycisk(){
  if(digitalRead(pin_plus)==LOW){
    if(tryb.stan()==1)poziom11.plus();
    if(tryb.stan()==2)poziom22.plus();
    if(tryb.stan()==3)poziom33.plus();
  }
  if(digitalRead(pin_minus)==LOW){
    if(tryb.stan()==1)poziom11.minus();
    if(tryb.stan()==2)poziom22.minus();
    if(tryb.stan()==3)poziom33.minus();
  }
  if(digitalRead(pin_tryb)==LOW){
    tryb.plus();
    if(tryb.stan()==0)poziom00.wyswietl();
    if(tryb.stan()==1)poziom11.wyswietl();
    if(tryb.stan()==2)poziom22.wyswietl();
    if(tryb.stan()==3)poziom33.wyswietl();
  }
  if(tryb.stan()!=0){
    int cykl=poziom33.stan()*20;
    DLUGOSC_WLACZENIA_W_DZIEN=(poziom11.stan()*cykl)/8;
    DLUGOSC_WYLACZENIA_W_DZIEN=cykl-DLUGOSC_WLACZENIA_W_DZIEN;
    DLUGOSC_WLACZENIA_W_NOCY=(poziom22.stan()*cykl)/8;
    DLUGOSC_WYLACZENIA_W_NOCY=cykl-DLUGOSC_WLACZENIA_W_NOCY;
    
  }
  
}
#endif


#define PIN_1_LCD 8
#define PIN_2_LCD 9
#define PIN_3_LCD 4
#define PIN_4_LCD 5
#define PIN_5_LCD 6
#define PIN_6_LCD 7
#define PIN_ANALOG_LCD 0
#define PIN_BUZZERA1 26
#define PIN_SWIATLA 24
//#define PIN_POTENCJOMETRU1 0
#define PIN_WIT 22 //numer pinu czujniak wilgotności i temperatury
#define PIN_NADAJNIKA1 28
#define PIN_TERMOMETRU1 30
#define PIN_PRZEKAZNIKA_1 32
#define PIN_PRZEKAZNIKA_2 34
//#define PIN_REGULATORA_CYKLU 0
//#define PIN_REGULATORA_WYPELNIENIA 1
//#define PIN_REGULATORA_NOCY 2
#define _DLUGOSC_WLACZENIA_W_NOCY 5
#define _DLUGOSC_WLACZENIA_W_DZIEN 10
#define _DLUGOSC_WYLACZENIA_W_NOCY 10
#define _DLUGOSC_WYLACZENIA_W_DZIEN 10
#define PIN_PLUS 50
#define PIN_MINUS 48
#define PIN_TRYB 46


#define PIN_TRYB1 13
#define PIN_TRYB2 12
#define PIN_TRYB3 11

#define PIN_DIODA_1 2
#define PIN_DIODA_2 9
#define PIN_DIODA_3 8
#define PIN_DIODA_4 7
#define PIN_DIODA_5 6
#define PIN_DIODA_6 5
#define PIN_DIODA_7 4
#define PIN_DIODA_8 3

//MENU menu1(PIN_TRYB1, PIN_TRYB2, PIN_TRYB3, PIN_DIODA_1, PIN_DIODA_2, PIN_DIODA_3, PIN_DIODA_4, PIN_DIODA_5, PIN_DIODA_6, PIN_DIODA_7, PIN_DIODA_8, PIN_PLUS, PIN_MINUS, PIN_TRYB);
//LCD_DISPLAY wyswietlacz(PIN_1_LCD, PIN_2_LCD, PIN_3_LCD, PIN_4_LCD, PIN_5_LCD, PIN_6_LCD, PIN_ANALOG_LCD);
CZUJNIKWIT czujnikwit(PIN_WIT);
CZUJNIKSWIATLA czujnikswiatla(PIN_SWIATLA);
//POTENCJOMETR potencjometr1(PIN_POTENCJOMETRU1);
//BUZZER buzzer1(PIN_BUZZERA1);
NADAJNIK nadajnik1(PIN_NADAJNIKA1);
TERMOMETR termometr1(PIN_TERMOMETRU1);
PRZEKAZNIK przekaznik1(PIN_PRZEKAZNIKA_1, PIN_PRZEKAZNIKA_2);
//POTENCJOMETR regulator(PIN_REGULATORA_CYKLU);
//POTENCJOMETR wypelnienie(PIN_REGULATORA_WYPELNIENIA);
//POTENCJOMETR noc(PIN_REGULATORA_NOCY);
//MENU menu1(PIN_TRYB1, PIN_TRYB2, PIN_TRYB3, PIN_DIODA_1, PIN_DIODA_2, PIN_DIODA_3, PIN_DIODA_4, PIN_DIODA_5, PIN_DIODA_6, PIN_DIODA_7, PIN_DIODA_8, PIN_PLUS, PIN_MINUS, PIN_TRYB);
void setup(){
  //Serial.begin(9600);
 // pinMode(10, INPUT);
}

int a=0;
unsigned long int czas;
unsigned long int pop;
unsigned long int pom;
unsigned long int prog_wylaczenia;
unsigned long int prog_wlaczenia;
void loop(){
  //Serial.println(analogRead(10));
 // menu1.przycisk();
  pop=czas;
  //regulator.odczyt();
  //wypelnienie.odczyt();
  //noc.odczyt();


  
  czas=millis()/1000;
  if(pop>czas){
    pom=0;
    pop=0;
    czas=0;
    return;
  }

  

  prog_wlaczenia=(czujnikswiatla.pora()=="DZIEN")? DLUGOSC_WLACZENIA_W_DZIEN+DLUGOSC_WYLACZENIA_W_DZIEN : DLUGOSC_WLACZENIA_W_NOCY+DLUGOSC_WYLACZENIA_W_NOCY;
  prog_wylaczenia=(czujnikswiatla.pora()=="DZIEN")? DLUGOSC_WLACZENIA_W_DZIEN : DLUGOSC_WLACZENIA_W_NOCY;

 /* Serial.println(czas);
  Serial.println(pom);
  Serial.println(czas-pom);
  //Serial.println()
  Serial.println(prog_wylaczenia);
  Serial.println(prog_wlaczenia);
  Serial.println("**");*/

  if(pom==0)pom=czas;
  else if(czas-pom<prog_wylaczenia){
    if(przekaznik1.stan1()==0)przekaznik1.wlacz1();
    if(przekaznik1.stan2()==0)przekaznik1.wlacz2();
  }
  else if(czas-pom<prog_wlaczenia){
    if(przekaznik1.stan1()==1)przekaznik1.wylacz1();
    if(przekaznik1.stan2()==1)przekaznik1.wylacz2();
  }
  else{
    pom=0;
  }
 
  
  //wyswietlacz.read_button();
  if(a==0){
  czujnikwit.odczyt();
  nadajnik1.wyslij(a, czujnikwit.wilgotnosc());
  }

  if(a==1){
  czujnikswiatla.odczyt();
  nadajnik1.wyslij(a, czujnikswiatla._pora());
  }
  //potencjometr1.odczyt();
  if(a==2){
  termometr1.odczyt();
  nadajnik1.wyslij(a, termometr1.podaj_temperature_10());
  }

  if(a==3){
   if(czujnikswiatla.pora()=="DZIEN") nadajnik1.wyslij(a, DLUGOSC_WLACZENIA_W_DZIEN);
   else{
    nadajnik1.wyslij(a, DLUGOSC_WLACZENIA_W_NOCY);
   }
  }

    if(a==4){
   if(czujnikswiatla.pora()=="DZIEN") nadajnik1.wyslij(a, DLUGOSC_WYLACZENIA_W_DZIEN);
   else{
    nadajnik1.wyslij(a, DLUGOSC_WYLACZENIA_W_NOCY);
   }
  }
  //wyswietlacz.lcd->setCursor(0,0);
  //wyswietlacz.lcd->print(czujnikwit.temperatura());
  //wyswietlacz.lcd->print("*C");
  //wyswietlacz.lcd->setCursor(0,1);
  /*wyswietlacz.lcd->print(czujnikwit.wilgotnosc());
  wyswietlacz.lcd->print("%");*/
  //wyswietlacz.lcd->print(a);
  /*Serial.print(czujnikwit.temperatura());
  Serial.print("*C\n");
  Serial.print(czujnikwit.wilgotnosc());
  Serial.print("%\n");
  Serial.print(czujnikswiatla.pora());
  Serial.print("\n");
  Serial.print(potencjometr1.procent());
  Serial.print("\n");
  if(czujnikswiatla.pora()=="DZIEN")buzzer1.wlacz();
  else{
    buzzer1.wylacz();
  }*/
  //nadajnik1.wyslij(1, czujnikwit.temperatura());
  //nadajnik1.wyslij(3, termometr1.podaj_temperature_10());
  
  
  delay(1000);
  ++a;
  a=a%5;
}

